﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SingletonSaveMenu : MonoBehaviour
{
    static SingletonSaveMenu Instance = null;

    IEnumerator DisableCanvas()//Is necessary that the canvas was actived at start in order to set his DontDestroyOnLoad active    
    {
        yield return new WaitForSeconds(0.002f);
        this.gameObject.SetActive(false);
    }
    private void Start()
    {
        StartCoroutine(DisableCanvas());
    }
    private void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        else
        {
            // Here we save our singleton instance
            Instance = this;

            // Furthermore we make sure that we don't destroy between scenes (this is optional)
            DontDestroyOnLoad(gameObject);
        }
    }
}
