﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ChangeSceneWithFade : MonoBehaviour
{
    public Image fade;
    public string e;
    public void CambioEscena(string es)
    {
        fade.CrossFadeAlpha(1, 1, true);
        StartCoroutine(ActivoFade(es));
    }

    IEnumerator ActivoFade(string e)
    {
        yield return new WaitForSeconds(1);
        SceneManager.LoadScene(e);
    }
}
