﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartPoint : MonoBehaviour
{
    //script para situar el lugar de inicio según la escena donde nos encontremos

    private RB player;
    private Camera camera;

    public string uuid;
    void Start()
    {
        player = FindObjectOfType<RB>();
        camera = FindObjectOfType<Camera>();
        if (!player.nextUuid.Equals(uuid))
        {
            return;
        }

            player.transform.position = this.transform.position; //posicionamos al jugador donde está el startpoint
        camera.transform.position = new Vector3(transform.position.x, transform.position.y, camera.transform.position.z);
    }

    
    void Update()
    {
        
    }
}
