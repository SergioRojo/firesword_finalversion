﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableCanvasWaitFS : MonoBehaviour
{
   IEnumerator DisableCanvas()//Is necessary that the canvas was actived at start in order to set his DontDestroyOnLoad active    
    {
        yield return new WaitForSeconds(0.002f);
        this.gameObject.SetActive(false);
    }
    private void Start()
    {
        StartCoroutine(DisableCanvas());
    }
}
