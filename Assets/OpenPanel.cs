﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenPanel : MonoBehaviour
{
    private UIManager canvas;

    private void Start()
    {
        canvas = FindObjectOfType<UIManager>();
    }

    public void OpenCanvas()
    {
        canvas.gameObject.SetActive(true);
        Debug.Log("Se ha abierto el canvas");
    }
}
