﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class WallJump : MonoBehaviour

{
    RB movement;
    public float distance = 6f;
    public float speed = 5f;
    bool wallJumping;
    float move = Input.GetAxisRaw("Horizontal");

    void Start()
    {
        movement = GetComponent<RB>();
    }

    void Update()
    {
        Physics2D.queriesStartInColliders = false;
        RaycastHit2D hit = Physics2D.Raycast(transform.position, Vector2.right * transform.localScale.x, distance);

        if ((CrossPlatformInputManager.GetButtonDown("JumpButton") || Input.GetKeyDown(KeyCode.JoystickButton1))  && !movement._grounded && hit.collider!= null)
        {
            
            GetComponent<Rigidbody2D>().velocity = new Vector2(speed *8,  hit.normal.y);
            wallJumping = true;
           transform.localScale = transform.localScale.x == 1 ? new Vector2(-1, 1) : Vector2.one;
        }
        else if (hit.collider!=null && wallJumping){
            wallJumping = false;
        }
    }

   
    void OnDrawGizmos()
    {
        Gizmos.color = Color.blue;
        Gizmos.DrawLine(transform.position, transform.position + Vector3.right * transform.localScale.x * distance);
    }
}
