﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using UnityStandardAssets.CrossPlatformInput;
public class RB : MonoBehaviour, IDamageable
{
    static RB Instance ;
    private void Awake()
    {
        // First we check if there are any other instances conflicting
        if (Instance != null)
        {
            // If that is the case, we destroy other instances
            Destroy(gameObject);
        }
        else
        {
            // Here we save our singleton instance
            Instance = this;

        }
    }
    public int scene;
    public static bool playerCreated;  //Variable for dont destroy on Load
    //variable for amount of diamonds
    public int gemas;
    //get handle to rigibody
    public Rigidbody2D _rigid;
    // private Rigidbody2D _rigidLeft;
    //private Rigidbody2D _rigidRight;
    [SerializeField]
    private float _jumpForce = 5.0f;
    // private float _jumpForceLeft = 3.0f;
    //private float _jumpForceRight = 3.0f;
    public bool _grounded = true; //we assumpt player is grounded when started
    [SerializeField]
    private LayerMask _groundLayer;
    [SerializeField]
    public float speed = 3.0f;
    [SerializeField]
    public int Health { get; set; } = 4;//Es necesario implementar la interfaz para que el jugador reciba daño de los enemigos

    private RBAnimation _playerAnim;
    private SpriteRenderer _playerSprite;
    private SpriteRenderer _swordArcSprite;
    private SpriteRenderer _slashFire;
    private SpriteRenderer _somersaultDust;
    private SpriteRenderer _somersaultFire;
    //private DontDestroyOnLoad _ddol;
    public GameObject weapon;
    public Button fireWandAttackButton;

    public bool isDead = false;
    private bool resetAttack = false;
    private bool canttouchthis = false;
    //private bool blink = false;
    protected Vector3 currentTarget;

    private UIManager GUI;
    public string nextUuid; //id para los puntos de aparicion del personaje


    private Renderer rend; //para cambiar el color al ser golpeado
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;

    private Touch theTouch;
    private Vector2 touchStartPosition, touchEndPosition;
    //private bool resetJumpNeeded = false; |||||| commented 'cause it's all about jumping, I preffer my own application, which include double jump

    void Start()
    {
        _rigid = GetComponent<Rigidbody2D>();
        // _rigidLeft = GetComponent<Rigidbody2D>();
        _playerAnim = GetComponent<RBAnimation>();
        _playerSprite = GetComponentInChildren<SpriteRenderer>();
        _swordArcSprite = transform.GetChild(1).GetComponent<SpriteRenderer>();
        _slashFire = transform.GetChild(2).GetComponent<SpriteRenderer>();
        _somersaultDust = transform.GetChild(3).GetComponent<SpriteRenderer>();
        _somersaultFire = transform.GetChild(4).GetComponent<SpriteRenderer>();
        currentTarget = transform.position;
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
        playerCreated = true;
        GUI = FindObjectOfType<UIManager>();
        GUI.gameObject.SetActive(true);
        if (SceneManager.GetActiveScene().name == "1.1")
        {
            nextUuid = "Scene1";
        }
        Debug.Log("Vidas al empezar la escena: " + Health);
    }


    //se usa para devolver el color original al Sprite
    private void ResetColor()
    {
            rend.material.color = defaultColor;
    }

    // Update is called once per frame 
    void Update()
    {
        Movement(); 
        Somersault();

        if (isDead == false)
        {
            CheckGrounded();
        }
        else
        {
            speed = 0;
            _grounded = false;
        }
        //needed here for double jump
        /* IEnumerator ResetJumpNeededRoutine()
   {
       yield return new WaitForSeconds(0.1f);
       resetJumpNeeded = false;
   } */

        //máximo de 1 ataque en x tiempo
        IEnumerator ResetAttackNeededRoutine()
        {
            yield return new WaitForSeconds(0.6f);
            resetAttack = false;
        }
        if (CrossPlatformInputManager.GetButtonDown("AttackButton") || Input.GetKeyDown(KeyCode.JoystickButton0)) //if (Input.GetKeyDown(KeyCode.JoystickButton0) && _grounded == true)
        {
            if (isDead == false)
            {
                _playerAnim.Attack();
                FindObjectOfType<AudioManager>().Play("Triz SwingingSword");
                if (resetAttack == false)
                {
                    _grounded = false;
                    resetAttack = true;              
                    StartCoroutine(ResetAttackNeededRoutine()); //startcoroutine sirve para inicializar un comportamiento Routine
                }
            }

        }
    }


    void Movement()
    {
        //horizontal input for left/right
        float move = Input.GetAxisRaw("Horizontal"); //CrossPlatformInputManager.GetAxis("Horizontal");      
        //else if move < 0 --> facing left
        if (move > 0 && isDead == false)
        {
            Flip(true);
        }
        else if (move < 0 && isDead == false)
        {
            Flip(false);
        }
        else if (move == 0)
        {
            _playerAnim.OneStep(true);
        }

        //if space key && grounded == true
        if ((CrossPlatformInputManager.GetButtonDown("JumpButton") || Input.GetKeyDown(KeyCode.JoystickButton1)) && _grounded == true) //(Input.GetKeyDown(KeyCode.JoystickButton1)<--botón de saltar         botón saltar mando --> (Input.GetKeyDown(KeyCode.JoystickButton1) ||
        {
            //jump
            _rigid.velocity = new Vector2(_rigid.velocity.x, _jumpForce);
            _grounded = false;
            _playerAnim.Jump(true);
            FindObjectOfType<AudioManager>().Play("Triz Jump");
        }
        _rigid.velocity = new Vector2(move * speed, _rigid.velocity.y);
        _playerAnim.Move(move);
        FindObjectOfType<AudioManager>().Play("Triz Walk");

    }

    void Somersault()
    {
        float move = CrossPlatformInputManager.GetAxis("Horizontal");

        if (Input.touchCount > 0 && move == 0)
            {
                theTouch = Input.GetTouch(0);

                if (theTouch.phase == TouchPhase.Began)
                {
                    touchStartPosition = theTouch.position;
                }

                else if (theTouch.phase == TouchPhase.Moved) //|| theTouch.phase == TouchPhase.Ended
            {
                    touchEndPosition = theTouch.position;

                    float looping = (touchEndPosition.x - touchStartPosition.x) /5;
                    float l1 = (float)1.8;
                    float l2 = (float)-1.8;


                if (looping > 0 && isDead == false )
                {
                    _playerAnim.Somersault(looping);
                   // FindObjectOfType<AudioManager>().Play("Triz Somersault");
                    _rigid.velocity = new Vector2(l1* speed, _rigid.velocity.y);
                    Flip(true);
                }
                else if (looping < 0 && isDead == false)
                {
                    _playerAnim.Somersault(looping);
                   // FindObjectOfType<AudioManager>().Play("Triz Somersault");
                    _rigid.velocity = new Vector2(l2 * speed, _rigid.velocity.y);
                    Flip(false);
                }                                                 
                }
                if (theTouch.phase == TouchPhase.Ended)
                {
                _playerAnim.Somersault(0);
                }
        } 
    }
    void CheckGrounded()
    {
        //2D raycast to the ground
        RaycastHit2D hitInfo = Physics2D.Raycast(transform.position, Vector2.down, 0.75f, _groundLayer.value);
        Debug.DrawRay(transform.position, Vector2.down, Color.green);
        if (hitInfo.collider != null)
        {
            _grounded = true;  
            _playerAnim.Jump(false); 
        }
    }

    public void Flip(bool faceRight)
    {
        if (faceRight == true)
        {
            _playerSprite.flipX = false;
            _swordArcSprite.flipX = false;
            _swordArcSprite.flipY = false;
            _slashFire.flipX = false;
            _slashFire.flipY = false;
            _somersaultDust.flipX = false;
            _somersaultDust.flipY = false;
            _somersaultFire.flipX = false;
            _somersaultFire.flipY = false;

            Vector3 newPos = _swordArcSprite.transform.localPosition;
            Vector3 newPosSomerDust = _somersaultDust.transform.localPosition;
            newPos.x = 0.8f;
            newPosSomerDust.x = -0.8f;
            _swordArcSprite.transform.localPosition = newPos;
            _somersaultDust.transform.localPosition = newPosSomerDust;
        }
        else if (faceRight == false)
        {
            _playerSprite.flipX = true;
            _swordArcSprite.flipX = true;
            _swordArcSprite.flipY = true;
            _slashFire.flipX = true;
            _slashFire.flipY = true;
            _somersaultDust.flipX = true;
            _somersaultDust.flipY = false;
            _somersaultFire.flipX = true;
            _somersaultFire.flipY = false;

            Vector3 newPos = _swordArcSprite.transform.localPosition;
            newPos.x = -0.8f;
            _swordArcSprite.transform.localPosition = newPos;
        }

    }

    IEnumerator Invulnerable() //tiempo para que Triz no sea herida
    {
        yield return new WaitForSeconds(1.5f);
        canttouchthis = false;
    }
    public void Damage()
    {
        if (Health < 1)
        {
            return; //si el jugador esta muerto, regresa y no se ejecuta el resto de código
        }
        if (canttouchthis == false)
        {
            Health--;
            UIManager.Instance.UpdateLives(Health); //actualizar las vidas en la UI de la pantalla
                                                    //_playerAnim.Hit();
            rend.material.color = colorToTurnTo;
            Invoke("ResetColor", 1.5f);
            UIManager.Instance.ChangeTrizImage(); //cambiar la cara de Triz cuando recibe daño
            UIManager.Instance.Jetazo();
            canttouchthis = true;
            StartCoroutine(Invulnerable());
        }
    
        if (Health < 1)
        {
            _playerAnim.Death();
            FindObjectOfType<AudioManager>().Play("Triz Death");
            isDead = true;
            scene = SceneManager.GetActiveScene().buildIndex; //HECHO HACE POCO PARA GUARDAR LA ESCENA DONDE TE MUERES, PARA HACER RESTART
            PlayerPrefs.SetInt("Nivel", scene);
            Invoke("ResetColor", .1f);
            Invoke("hasJamado", 3.5f);      
        }
    }
    IEnumerator ResetWeaponAttack()
    {
        yield return new WaitForSeconds(1.5f);
        fireWandAttackButton.gameObject.SetActive(true);
    }
   IEnumerator FireWandSpriteReset() //Rutina para resetear el sprite del bastón
    {
        yield return new WaitForSeconds(0.7f);
        weapon.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = true;
    }
    
    /*IEnumerator ResetWeaponAttackNeededRoutine() //Rutina para resetear el tiempo de ataque del bastón de fuego
    {
        yield return new WaitForSeconds(1.5f);
        resetWeaponAttack = false;
    }*/

    public void FireWandAttack() //Animación ataque FireWand
    {
       // if (resetWeaponAttack == false)
        //{
            Flip(true);
            _playerAnim.FireWandAttack();
            weapon.transform.GetChild(1).GetComponent<SpriteRenderer>().enabled = false;
            fireWandAttackButton.gameObject.SetActive(false);
            StartCoroutine(FireWandSpriteReset());
            StartCoroutine(ResetWeaponAttack());
       // }
    
    }

    public void AddGems(int amount) //va sumando las gemas que recolecta  player
    {
        gemas += amount;
        UIManager.Instance.UpdateGemCount(gemas);
        FindObjectOfType<AudioManager>().Play("Triz Obtains Gem");
    }

    public void hasJamado() //GameOver invoke
    {
        //gameObject.SetActive(false);
       // GUI.gameObject.SetActive(false);
        SceneManager.LoadScene("GameOver");

    }

}
