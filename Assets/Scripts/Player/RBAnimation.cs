﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RBAnimation : MonoBehaviour
{
    private Animator _anim;
    //reference to sword animation
    private Animator _swordAnimation;
    private Animator _slashFire;
    private Animator _somersaultDust;
    private Animator _somersaultFire;

    //handle to animator
    void Start()
    {
        _anim = GetComponentInChildren<Animator>();
        _swordAnimation = transform.GetChild(1).GetComponent<Animator>();
        _slashFire = transform.GetChild(2).GetComponent<Animator>();
        _somersaultDust = transform.GetChild(3).GetComponent<Animator>();
        _somersaultFire = transform.GetChild(4).GetComponent<Animator>();
    }

    public void Move(float move)
    {
        _anim.SetFloat("Move", Mathf.Abs(move));
    }
    public void Somersault(float looping)
    {
        _anim.SetFloat("Somersault", Mathf.Abs(looping));
        if (looping > 0 )
        {
            _somersaultDust.SetTrigger("SomersaultDust");
            _somersaultFire.SetTrigger("SomersaultFire");
        }
        if (looping < 0)
        {
            _somersaultFire.SetTrigger("SomersaultFire");
        }
    }

    public void Jump(bool jumping)
    {
        _anim.SetBool("Jumping", jumping);
    }

    public void OneStep(bool onestep)
    {
        _anim.SetBool("OneStep", onestep);
    }

    public void Attack()
    {
        _anim.SetTrigger("Attack");
        _swordAnimation.SetTrigger("SwordAnimation");
        _slashFire.SetTrigger("SwordAnimation");

    }

    public void FireWandAttack()
    {
        _anim.SetTrigger("FireWandAttack");
    }

    public void Death()
    {
        _anim.SetTrigger("Death");
    }
}
