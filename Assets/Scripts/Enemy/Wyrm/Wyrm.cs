﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Wyrm : Enemy, IDamageable
{
    public int Health { get; set; }
    private PolygonCollider2D _collider; //para quitar el collider de los cadaveres
 
    private Renderer rend; //para cambiar el color al ser golpeado
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;
    
    private bool resetHit = false;
    public float visionRadius;
    public float visionRadiusShake;


    protected Vector3 currentTarget;

    public override void Init()
    {
        base.Init();
        Health = base.health;
        _collider = GetComponent<PolygonCollider2D>();
        currentTarget = transform.position;
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
    }

    //se usa para devolver el color original al Sprite
    void ResetColor()
    {
        rend.material.color = defaultColor;
    }


    public override void Update()
    {
       
        Vector3 direction = player.transform.localPosition - transform.localPosition;
        
        float distance = Vector3.Distance(player.transform.localPosition, transform.localPosition);

        //orientacion del enemigo cuando es golpeado por el jugador
      

        if (isDead == false)
        {
            //si distancia < shake, se invoca al metodo que mueve la camara
            if (distance < visionRadiusShake && distance > visionRadius)
            {
                FindObjectOfType<CameraShake>().ShakeItMedium();
            }
            if (distance < visionRadius)
                {
                    FindObjectOfType<AudioManager>().Play("Wyrm Surprise");
                    anim.SetTrigger("Surprise");
                    anim.SetTrigger("Attack");
                }
                else
                {
         
                    anim.SetBool("Idle", true);
                }
            
        }
        if (direction.x > 0)
        {
            sprite.flipX = true;
        }
        else if (direction.x < 0)
        {
            sprite.flipX = false;
        }

        if (direction.x > 0)
        {
            sprite.flipX = true;
        }
        else if (direction.x < 0)
        {
            sprite.flipX = false;
        }

        Debug.DrawLine(transform.position, currentTarget, Color.green);
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);

        Gizmos.color = Color.white;
        Gizmos.DrawWireSphere(transform.position, visionRadiusShake);
    }

    //Time for reset Wyrm 'Hit', giving our friend time to hide
    IEnumerator ResetHitNeededRoutine()
    {
        yield return new WaitForSeconds(1f);
        resetHit = false;
    }

    public void Damage()
    {

        if (isDead == true) // este código se usa para evitar que reciban daño una vez muertos
            return;
        if (resetHit == false)
        {
            Health--;
            anim.SetTrigger("Hit");
            FindObjectOfType<AudioManager>().Play("Wyrm Hit");
            rend.material.color = colorToTurnTo;
            Invoke("ResetColor", .2f);
            anim.SetBool("Idle", true);
            resetHit = true;
            StartCoroutine(ResetHitNeededRoutine());
        }




        if (Health < 1)
        {
            isDead = true;
            GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
            gem.GetComponent<Gem>().gems = base.gems;  //para asignarle el valor que le hemos puesto por defecto en gems
            _collider.enabled = false; //quitar el collider
            Invoke("ResetColor", .1f); // se le devuelve el color original al morir
            Destroy(gameObject);
        }
    }
}
