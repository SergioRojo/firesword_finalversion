﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class Enemy : MonoBehaviour
{

    public GameObject gemPrefab; //para las gemas que sueltan
    [SerializeField]
    protected int health;
    [SerializeField]
    protected float speed;
    [SerializeField]
    protected int gems;
    [SerializeField]
    protected Transform pointA, pointB;

    protected Vector3 currentTarget;
    protected Animator anim;
    protected SpriteRenderer sprite;
    protected bool isDead = false;

    //isHit sirve para congelar al enemigo cuando este es golpeado o anda cerca del player(actitud de combate)
    protected bool isHit = false;
    protected RB player;

    //referencia a los efectos de animación 
    private Animator _anim;
       
    public virtual void Init()
    {
        anim = GetComponentInChildren<Animator>();
        sprite = GetComponentInChildren<SpriteRenderer>();
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
        currentTarget = transform.position;
        
    }

    private void Start()
    {
        Init();
    }

    public virtual void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") && anim.GetBool("InCombat") == false)
        {
            return;
        }

        if (isDead == false)
        Movement();
    }

    public virtual void Movement()
    {

        if (currentTarget == pointA.position)
        {
            sprite.flipX = false;
        }
        else
        {
            sprite.flipX = true;
        }
        if (transform.position == pointA.position) ///aquí sería el b, pero poniendo _scorpion.flipX = false; primero y luego true no queda mal. Originalmente sería primero  if (_currentTarget == pointB.position) seguramente
        {
            currentTarget = pointB.position;
            anim.SetTrigger("Idle");

        }
        else if (transform.position == pointB.position)
        {
            currentTarget = pointA.position;
            anim.SetTrigger("Idle");
        }

        if (isHit == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, speed * Time.deltaTime);
        }

        //chequea la distancia entre  player y enemy, reanuda walk
        float distance = Vector3.Distance(transform.localPosition, player.transform.localPosition);
        if (distance > 5.0f)
        {
            isHit = false;
            anim.SetBool("InCombat", false);
        }

        //orientacion del enemigo cuando es golpeado por el jugador
        Vector3 direction = player.transform.localPosition - transform.localPosition;

        if (direction.x > 0 && anim.GetBool("InCombat") == true)
        {
            sprite.flipX = true;
        }
        else if (direction.x < 0 && anim.GetBool("InCombat") == true)
        {
            sprite.flipX = false;
        }

    }


}
