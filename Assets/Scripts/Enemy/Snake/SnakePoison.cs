﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakePoison : MonoBehaviour
{
    protected RB player;
    Vector3 direction;
    protected SpriteRenderer sprite;


    void Start()
    {
        Destroy(this.gameObject, 3.3f);
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
        direction = player.transform.localPosition - transform.localPosition;
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

   

    void Update()
    {
        
        if (direction.x > 0)
        {
            transform.Translate(Vector3.right * 5 * Time.deltaTime);
            sprite.flipX = true;
        }
        else if (direction.x < 0)
        {
            transform.Translate(Vector3.left * 5 * Time.deltaTime);
            sprite.flipX = false;
        }
        

    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            IDamageable hit = other.GetComponent<IDamageable>();
            

            if (hit != null)
            {
                hit.Damage();
                Destroy(this.gameObject);
            }
        }
    }
}
