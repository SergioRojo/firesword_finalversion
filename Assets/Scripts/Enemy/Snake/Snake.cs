﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Snake : Enemy, IDamageable
{
    public int Health { get; set; }
    public float visionRadius;
    private BoxCollider2D _collider; //para quitar el collider de los cadaveres

    private Renderer rend; //para cambiar el color al ser golpeado
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;

    public GameObject SnakeShot;

    public override void Init()
    {
        base.Init();
        Health = base.health; //el valor que le indicas por defecto
        _collider = GetComponent<BoxCollider2D>();
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
    }

    //se usa para devolver el color original al Sprite
    void ResetColor()
    {
        rend.material.color = defaultColor;
    }

    //para ver la esfera de radio de visión
    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

    public override void Movement()
    {
        base.Movement();

    }

    public void Attack()
    {
        Instantiate(SnakeShot, transform.position, Quaternion.identity);
    }
    public override void Update()
    {
        //si la distancia al jugador < radio de visión, el objetivo será el

        float distance = Vector3.Distance(player.transform.localPosition, transform.localPosition);
        Vector3 direction = player.transform.localPosition - transform.localPosition;


        //movemos al enemigo en dirección a su target
        if (isDead == false)
        {
            if (distance < visionRadius)
            {
                currentTarget = player.transform.position;
                anim.SetTrigger("Snakeprise");
                if (direction.x > 0)
                {
                    sprite.flipX = true;
                }
                else if (direction.x < 0)
                {
                    sprite.flipX = false;
                }
            }

            if (direction.x > 0 )
            {
                sprite.flipX = true;
            }
            else if (direction.x < 0 )
            {
                sprite.flipX = false;
            }
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, fixedSpeed);

        }

        Debug.DrawLine(transform.position, currentTarget, Color.green);
    }


    public void Damage()
    {
        if (isDead == true)
            return;

        Health--;
        anim.SetTrigger("Hit");
        rend.material.color = colorToTurnTo;
        isHit = true;
        anim.SetBool("InCombat", true);
        Invoke("ResetColor", .2f);


        if (Health < 1)
        {
            isDead = true;
            anim.SetTrigger("Death");

            //soltar las gemas
            GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
            gem.GetComponent<Gem>().gems = base.gems; //para asignarle el valor que le hemos puesto por defecto en gems
            _collider.enabled = false; //quitar el collider
            Invoke("ResetColor", .1f); // se le devuelve el color original al morir
        }
    }

}
