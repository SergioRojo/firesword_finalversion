﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Mimic : Enemy, IDamageable
{
    public int Health { get; set; }
    private PolygonCollider2D _collider; //para quitar el collider de los cadaveres
    //private Animator _blueFire;
    private Animator _anim;

    private Renderer rend; //para cambiar el color al ser golpeado

    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;

    public override void Init()
    {
        base.Init();
        Health = base.health;
        _collider = GetComponent<PolygonCollider2D>();
        _anim = GetComponentInChildren<Animator>();
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
        // _blueFire = transform.GetChild(1).GetComponent<Animator>();
    }

    //se usa para devolver el color original al Sprite
    void ResetColor()
    {
        rend.material.color = defaultColor;
    }
    public override void Movement()
    {
        float distance = Vector3.Distance(player.transform.localPosition, transform.localPosition);
        //orientacion del enemigo cuando es golpeado por el jugador
        Vector3 direction = player.transform.localPosition - transform.localPosition;

        // Debug.Log("Lado: " + direction.x); mide la distancia al jugador

        if (direction.x > 0 && anim.GetBool("InCombat") == true)
        {
            sprite.flipX = true;
        }
        else if (direction.x < 0 && anim.GetBool("InCombat") == true)
        {
            sprite.flipX = false;
        }
    }

    public void Damage()
    {
        if (isDead == true) // este código se usa para evitar que reciban daño una vez muertos
            return;
        Health--;
        //anim.SetTrigger("Hit");
        rend.material.color = colorToTurnTo;
        isHit = true;
        anim.SetBool("InCombat", true);
        Invoke("ResetColor", .8f);
        if (Health < 1)
        {
            isDead = true;
            anim.SetTrigger("Death");

            //soltar las gemas
            GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
            gem.GetComponent<Gem>().gems = base.gems; //para asignarle el valor que le hemos puesto por defecto en gems
            _collider.enabled = false; //quitar el collider
            Invoke("ResetColor", .1f); // se le devuelve el color original al morir
        }
    }
}