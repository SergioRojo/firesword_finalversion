﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion_AttackFX : MonoBehaviour
{
    private Scorpion _scorpion;

    private Animator _anim;
    //reference to claw animation
    private Animator _clawFX;


    private void Start()
    {
        _anim = GetComponentInChildren<Animator>();
        _scorpion = transform.parent.GetComponent<Scorpion>();
        _clawFX = transform.GetChild(1).GetComponent<Animator>();
    }
    public void Attack()
    {
        _clawFX.SetTrigger("FXAnimation");
    }
}
