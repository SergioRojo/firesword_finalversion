﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bat : Enemy, IDamageable
{
    public int Health { get; set; }
    public float visionRadius;
    private PolygonCollider2D _collider; //para quitar el collider de los cadaveres

    private Renderer rend; //para cambiar el color al ser golpeado
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;
   // private Rigidbody2D _rigid;

  /*  private void Start()
    {
        _rigid = GetComponent<Rigidbody2D>();
    }*/
    public override void Init()
    {
        base.Init();
    Health = base.health; //el valor que le indicas por defecto
        _collider = GetComponent<PolygonCollider2D>();
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
    }

    //se usa para devolver el color original al Sprite
    void ResetColor()
    {
        rend.material.color = defaultColor;
    }

    public override void Update()
   {
       //si la distancia al jugador < radio de visión, el objetivo será el

       float distance = Vector3.Distance(player.transform.localPosition, transform.localPosition);
        Vector3 direction = player.transform.localPosition - transform.localPosition;
       
        
        //movemos al enemigo en dirección a su target
        if (isDead == false)
        {
            if (distance < visionRadius)
            {
                currentTarget = player.transform.position;
                if (direction.x > 0)
                {
                    sprite.flipX = true;
                }
                else if (direction.x < 0)
                {
                    sprite.flipX = false;
                }
            }

            if (direction.x > 0 && anim.GetBool("InCombat") == true)
            {
                sprite.flipX = true;
            }
            else if (direction.x < 0 && anim.GetBool("InCombat") == true)
            {
                sprite.flipX = false;
            }
            float fixedSpeed = speed * Time.deltaTime;
            transform.position = Vector3.MoveTowards(transform.position, currentTarget, fixedSpeed);

        }

        Debug.DrawLine(transform.position, currentTarget, Color.green);
   }

   //para ver la esfera de radio de visión
   void OnDrawGizmos()
   {
      Gizmos.color = Color.yellow;
      Gizmos.DrawWireSphere(transform.position, visionRadius);
   }

    public override void Movement()
    {
        base.Movement();
      
    }

    IEnumerator Die()
    {
        yield return new WaitForSeconds(1f);
        Destroy(this.gameObject);
    }
    public void Damage()
    {
        if (isDead == true) // este código se usa para veitar que reciban daño una vez muertos
            return;

        Health--;
        anim.SetTrigger("Hit");
        rend.material.color = colorToTurnTo;
        isHit = true;
        anim.SetBool("InCombat", true);
        Invoke("ResetColor", .8f);

        if (Health < 1)
        {
            isDead = true;
            anim.SetTrigger("Death");
           
            //soltar las gemas
            GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
            gem.GetComponent<Gem>().gems = base.gems; //para asignarle el valor que le hemos puesto por defecto en gems
            _collider.enabled = false; //quitar el collider
            Invoke("ResetColor", .1f); // se le devuelve el color original al morir
            StartCoroutine(Die());
           // _rigid.gravityScale = 2; //le otorgamos gravedad para que el cadaver no se quede volando
        }
    }
}

