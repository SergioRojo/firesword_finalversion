﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scorpion : Enemy, IDamageable
{
    public int Health { get; set; }
    private PolygonCollider2D _collider; //para quitar el collider de los cadaveres
    //private Animator _clawAnimation;

    private Renderer rend; //para cambiar el color al ser golpeado
    [SerializeField]
    private Color colorToTurnTo = Color.white;
    private Color defaultColor;

    private Vector3 playerPos;
    private Vector3 newXPos;

    //private Scorpion_AttackFX _sfx;

    public override void Init()
    {
        base.Init();
        Health = base.health;
        _collider = GetComponent<PolygonCollider2D>();
       // _clawAnimation = transform.GetChild(1).GetComponent<Animator>();
        rend = GetComponentInChildren<Renderer>();
        defaultColor = rend.material.color;
        
    }

    //se usa para devolver el color original al Sprite
    void ResetColor()
    {
        rend.material.color = defaultColor;
    }


    public override void Update()
    {
        if (anim.GetCurrentAnimatorStateInfo(0).IsName("Idle") && anim.GetBool("InCombat") == false)
        {
            return;
        }

        if (isDead == false)
            Movement();

        if (isHit == true && isDead == false)
        {
            playerPos = player.transform.position;
        float fixedSpeed = speed * Time.deltaTime;
            newXPos = new Vector3(playerPos.x, transform.position.y, 0);   // Disregard the player's y and z position.
            transform.position = Vector3.MoveTowards(transform.position, newXPos, 0.01f);
        }
    }
    public override void Movement()
    {
        base.Movement();
      
    }

    public void Damage()
    {
        if (isDead == true) // este código se usa para evitar que reciban daño una vez muertos
            return;
        Health--;
       // sr.material = matWhite;
        anim.SetTrigger("Hit");
        rend.material.color = colorToTurnTo;
        isHit = true;
        anim.SetBool("InCombat", true);
        //_clawAnimation.SetTrigger("FXAnimation");
        Invoke("ResetColor", .8f);

        if (Health < 1)
        {
            isDead = true;
            anim.SetTrigger("Death");
            GameObject gem = Instantiate(gemPrefab, transform.position, Quaternion.identity) as GameObject;
            gem.GetComponent<Gem>().gems = base.gems;  //para asignarle el valor que le hemos puesto por defecto en gems
            _collider.enabled = false; //quitar el collider
            Invoke("ResetColor", .1f); // se le devuelve el color original al morir

        }
    }
    
}

