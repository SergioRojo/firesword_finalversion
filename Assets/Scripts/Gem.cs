﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Gem : MonoBehaviour
{
    public int gems = 1;

    void OnTriggerEnter2D(Collider2D other)
    {
        if(other.CompareTag("Player"))
        {
            RB player = other.GetComponent<RB>();

            if (player != null)
            {
                player.AddGems(gems);
                Destroy(this.gameObject);

            }

        }
    }
}
