﻿using System.Collections.Generic;
using UnityEngine;
using System;//permite trabajar con String.Format
using System.Data;
using System.IO;
using Mono.Data.Sqlite;

public class CreditsManager : MonoBehaviour
{
    string rutaDB;
    string conexion;
    public GameObject puntosPrefab;
    public Transform puntosPadre;
   // public int topRank; //variable que indica el número de registros que se van a mostrar en el ranking
    public int limiteRanking;

    IDbConnection conexionDB;
    IDbCommand comandosDB;
    IDataReader leerDatos;
    string nombreDB = "RankingDB.db";
    private List<Credits> credits = new List<Credits>();

      void Start()
     {
        /*
        InsertarCredits("Vagelio", "Triz", "All", "Sprite");
        InsertarCredits("Melissa Krautheim", "FireSword", "All", "Sprite");
        InsertarCredits("Pineda", "Cecil", "Entrance", "Sprite");
        InsertarCredits("Ocean's Dream", "Cecil Face", "Entrance", "Sprite");
        InsertarCredits("Aekashics", "Mimic", "Entrance", "Sprite");
        InsertarCredits("Pineda", "Al", "Archaic Ruins", "Sprite");
        InsertarCredits("Ocean's Dream", "Al Face", "Archaic Ruins", "Sprite");
        InsertarCredits("Aekashics", "Snake Cobrens", "Archaic Ruins", "Sprite");
        InsertarCredits("Blacky91", "Jormungandr", "Archaic Ruins", "Sprite");
        InsertarCredits("Aekashics", "Wyrm", "Cave tunnels", "Sprite");
        InsertarCredits("The Toad", "The Valley of Orodrin", "Main Menu", "Song");
        InsertarCredits("The Toad", "Cave of Illusions", "Entrance", "Song");
        InsertarCredits("The Toad", "Arpegy 09", "Archaic Ruins", "Song");
        InsertarCredits("The Toad", "Arpegy 06", "Cave Tunnels", "Song"); */
    }

    void AbrirDB()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) //para Windows / OS
        {
            rutaDB = Application.dataPath + "/StreamingAssets/" + nombreDB;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            rutaDB = Application.persistentDataPath + "/" + nombreDB;
            // Comprobar si el archivo se encuentra almacenado en persistant data
            if (!File.Exists(rutaDB))
            {
                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + nombreDB);
                while (!loadDB.isDone)
                { }
                File.WriteAllBytes(rutaDB, loadDB.bytes);
            }
        }
        // Almaceno el archivo en load db 
        // Copio el archivo a persistant data
        conexion = "URI=file:" + rutaDB;
        conexionDB = new SqliteConnection(conexion);
        conexionDB.Open();
    }

    void ObtenerCredits()
    {
        credits.Clear();
        AbrirDB();
        // Crear la consulta
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = "SELECT * FROM Credits";
        comandosDB.CommandText = sqlQuery;

        // Leer la base de datos
        leerDatos = comandosDB.ExecuteReader();

        while (leerDatos.Read())
        {

            credits.Add(new Credits(leerDatos.GetString(0), leerDatos.GetString(1), leerDatos.GetString(2), leerDatos.GetString(3))); //se añaden las columnas de artist, asset,  level y type

        }
        //así cada vez que lea un dato lo cierra, da menos problemas
        leerDatos.Close();
        leerDatos = null;
        CerrarDB();
       // credits.Sort();
    }

    public void InsertarCredits(string artist, string asset, string level, string type)
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = String.Format("insert into Credits(Artist, Asset, Level, Type) values(\"{0}\",\"{1}\",\"{2}\",\"{3}\")", artist, asset, level, type);
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }
    void BorrarCredits(string asset)
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = "delete from Credits where Asset = \"" + asset + "\"";
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }

    public void OrdenarPorTipo(string type) //permite al usuario ordenar los créditos según sean canciones o sprites
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = "select * from Credits where Type = \"" + type + "\"";
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }
    public void MostrarCredits()
    {
        ObtenerCredits();
        for (int i = 0; i < 100; i++) //he puesto 100 de momento, no creo que haya 100 creditos
        {
            if (i < credits.Count) //para que no muestre más cantidad de registros que los que hay guardados, si no ponemos esto no da error pero vuelve a mostrar registros repetidos 
            {
                GameObject tempPrefab = Instantiate(puntosPrefab, transform.position, Quaternion.identity);
                tempPrefab.transform.SetParent(puntosPadre);
                tempPrefab.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1); //redimensiona los clones del prefab, sin esta línea su escala aumenta a (3.4, 3.4, 3.4)
                Credits rankTemp = credits[i];
                tempPrefab.GetComponent<CreditsScript>().PonerPuntos(rankTemp.Artist, rankTemp.Asset, rankTemp.Level, rankTemp.Type);
            }
        }
    }

    public void BorrarPuntosExtra()
    {
        ObtenerCredits();
        if (limiteRanking <= credits.Count) //elIminalos registros en la bbdd que no se van a mostrar, para no sobrecargarla
        {
            credits.Reverse();
            int diferencia = credits.Count - limiteRanking;
            AbrirDB();
            comandosDB = conexionDB.CreateCommand();
            for (int i = 0; i < diferencia; i++)
            {
                //borrar por ID en la posición del ranking
                string sqlQuery = "delete from Credits where Asset = \"" + credits[i].Asset + "\"";
                comandosDB.CommandText = sqlQuery;
                comandosDB.ExecuteScalar();
            }
            CerrarDB();
        }
    }

    // Cerrar las conexiones
    void CerrarDB()
    {
        comandosDB.Dispose();
        comandosDB = null;
        conexionDB.Close();
        conexionDB = null;
    }
}



