﻿using System.Collections.Generic;
using UnityEngine;
using System; //recuerda que es necesario poner el System para que te reconozca el tipo DateTime

public class Credits : MonoBehaviour  //IComparable es necesario para el compareTo
{
    public string Artist { get; set; }
    public string Asset { get; set; }
    public string Level { get; set; }
    public string Type { get; set; }

    public Credits(string artist, string asset, string level, string type)
    {
        this.Artist = artist;
        this.Asset = asset;
        this.Level = level;
        this.Type = type;
    }

    //CompareTo se usa para un ranking
    /*public int CompareTo(Credits other)
    {
        if (other.Score > this.Score)
        {
            return 1;
        }
        else if (other.Score < this.Score)
        {
            return -1;
        }
        else if (other.Date > this.Date)
        {
            return -1;
        }
        else if (other.Date < this.Date)
        {
            return 1;
        }
        return 0;
    }*/
}
