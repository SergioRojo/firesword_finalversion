﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;

public class DropDownType : MonoBehaviour
{
   private CreditsManager cm;

    private void Start()
    {
        cm = FindObjectOfType<CreditsManager>();
    }

    public void HandleInputData(Dropdown drop)
    {
        if (drop.value == 0)
        {
            Debug.Log("Tipo todos");
            cm.MostrarCredits();
        }
        if (drop.value == 1)
        {
            Debug.Log("Tipo sprites");
            cm.OrdenarPorTipo("Sprite");
        }
        if (drop.value == 2)
        {
            Debug.Log("Tipo canciones");
            cm.OrdenarPorTipo("Song");
        }
    }
}
