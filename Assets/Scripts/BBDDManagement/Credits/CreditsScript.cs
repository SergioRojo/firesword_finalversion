﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CreditsScript : MonoBehaviour
{
    public GameObject Artist;
    public GameObject Asset;
    public GameObject Level;
    public GameObject Type;

    //convertir a String
    public void PonerPuntos(string artist, string asset, string level, string type)
    {
        this.Artist.GetComponent<Text>().text = artist;
        this.Asset.GetComponent<Text>().text = asset;
        this.Level.GetComponent<Text>().text = level;
        this.Type.GetComponent<Text>().text = type;

    }
}
