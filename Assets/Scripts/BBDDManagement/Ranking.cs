﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System; //recuerda que es necesario poner el System para que te reconozca el tipo DateTime

public class Ranking : IComparable<Ranking>  //IComparable es necesario para el compareTo
{
    public int Id { get; set; }
    public string Name { get; set; }
    public int Score { get; set; }
    public DateTime Date { get; set; }
    
    public Ranking(int id, string name, int score, DateTime date)
    {
        this.Id = id;
        this.Name = name;
        this.Score = score;
        this.Date = date;
    }

    public int CompareTo(Ranking other)
    {
        if (other.Score > this.Score)
        {
            return 1;
        }
        else if (other.Score < this.Score)
        {
            return -1;
        }
        else if (other.Date > this.Date)
        {
            return -1;
        }
        else if (other.Date < this.Date)
        {
            return 1;
        }
        return 0;
    }
}
