﻿using System;//permite trabajar con String.Format
using System.Collections.Generic;
using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine;



public class RankingManager : MonoBehaviour
{
    string rutaDB;
    string conexion;
    public GameObject puntosPrefab;
    public Transform puntosPadre;
    public int topRank; //variable que indica el número de registros que se van a mostrar en el ranking
    public int limiteRanking;

    IDbConnection conexionDB;
    IDbCommand comandosDB;
    IDataReader leerDatos;
    string nombreDB = "RankingDB.db";
    private List<Ranking> rankings = new List<Ranking>();

    private void Start()
    {
       
    }
    // Start is called before the first frame update
    //void Start()
    //{
    //    //InsertarPuntos("Joni", 80);
    //   /* InsertarPuntos("Boster", 94);
    //    InsertarPuntos("Pusans", 94);
    //    InsertarPuntos("Donaldo Eldritch", 250);
    //    InsertarPuntos("Nazario Legendario", 220);
    //    InsertarPuntos("El rey del cachopo", 75);
    //    InsertarPuntos("Jurolo", 91);
    //    InsertarPuntos("Silvia", 89);
    //    InsertarPuntos("Cthun", 00);
    //    //InsertarPuntos("Cecil", 89);*/
    //    //BorrarPuntos(6);
    //    //ObtenerRanking();
    //    BorrarPuntosExtra();
    //    MostrarRanking();
    //}

    void AbrirDB()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) //para Windows / OS
        {
            rutaDB = Application.dataPath + "/StreamingAssets/" + nombreDB;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            rutaDB = Application.persistentDataPath + "/" + nombreDB;
            // Comprobar si el archivo se encuentra almacenado en persistant data
            if (!File.Exists(rutaDB))
            {
                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + nombreDB);
                while (!loadDB.isDone)
                { }
                File.WriteAllBytes(rutaDB, loadDB.bytes);
            }
        }
        // Almaceno el archivo en load db 
        // Copio el archivo a persistant data
        conexion = "URI=file:" + rutaDB;
        conexionDB = new SqliteConnection(conexion);
        conexionDB.Open();
    }

    public void ObtenerRanking()
    {
        rankings.Clear();
        AbrirDB();
        // Crear la consulta
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = "SELECT * FROM Ranking";
        comandosDB.CommandText = sqlQuery;

        // Leer la base de datos
        leerDatos = comandosDB.ExecuteReader();

        while (leerDatos.Read())
        {
            rankings.Add(new Ranking(leerDatos.GetInt32(0), leerDatos.GetString(1), leerDatos.GetInt32(2), leerDatos.GetDateTime(3)));

        }
        //así cada vez que lea un dato lo cierra, da menos problemas
        leerDatos.Close();
        leerDatos = null;
        CerrarDB();
        rankings.Sort();
    }

    public void InsertarPuntos(string n, int s)
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery = String.Format("insert into Ranking(Name,Score) values(\"{0}\",\"{1}\")", n, s);
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }
    void BorrarPuntos(int id)
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        string sqlQuery ="delete from Ranking where PlayerId = \"" +id+"\"";
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }

    public void MostrarRanking()
    {
        ObtenerRanking();
        for (int i = 0; i < topRank; i++) 
        {
            if (i < rankings.Count) //para que no muestre más cantidad de registros que los que hay guardados, si no ponemos esto no da error pero vuelve a mostrar registros repetidos 
            {
                GameObject tempPrefab = Instantiate(puntosPrefab, transform.position, Quaternion.identity);
                tempPrefab.transform.SetParent(puntosPadre);
                tempPrefab.GetComponent<RectTransform>().localScale = new Vector3(1, 1, 1); //redimensiona los clones del prefab, sin esta línea su escula aumenta a (3.4, 3.4, 3.4)
                Ranking rankTemp = rankings[i];
                tempPrefab.GetComponent<RankingScript>().PonerPuntos("#" + (i + 1).ToString(), rankTemp.Name, rankTemp.Score.ToString());
            }
        }
    }

    public void BorrarPuntosExtra()
    {
        ObtenerRanking();
        if (limiteRanking <= rankings.Count) //elIminalos registros en la bbdd que no se van a mostrar, para no sobrecargarla
        {
            rankings.Reverse();
            int diferencia = rankings.Count - limiteRanking;
            AbrirDB();
            comandosDB = conexionDB.CreateCommand();
            for (int i = 0; i < diferencia; i++)
            {
                //borrar por ID en la posición del ranking
                string sqlQuery = "delete from Ranking where PlayerId = \"" + rankings[i].Id + "\"";
                comandosDB.CommandText = sqlQuery;
                comandosDB.ExecuteScalar();
            }
           CerrarDB();
        }
    }

    // Cerrar las conexiones
    void CerrarDB()
    {
        comandosDB.Dispose();
        comandosDB = null;
        conexionDB.Close();
        conexionDB = null;
    }
}

