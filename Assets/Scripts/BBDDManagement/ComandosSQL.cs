﻿using System; 
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using System.Data;
using System.IO;
using Mono.Data.Sqlite;
using UnityEngine.UI;

public class ComandosSQL : MonoBehaviour
{
    string rutaDB;
    string strConexion;
    string DBFileName = "chinook.db";
 

    IDbConnection dbConnection;
    IDbCommand dbCommand;
    IDataReader reader;

    // Use this for initialization
    void Start()
    {
        AbrirDB();
        //ComandoSelect("*", "albums");
        ComandoWHERE_AND("CustomerId", "FirstName", "LastName", "Country", "customers", "Country", "=", "Brazil", "CustomerId", ">", "10");
        CerrarDB();
    }

    void AbrirDB()
    {
        // Crear y abrir la conexion
        //Compruebo que plataforma es
        // Si es PC mantengo la ruta
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor)
        {
            rutaDB = Application.dataPath + "/StreamingAssets/" + DBFileName;
        }
        else if (Application.platform == RuntimePlatform.IPhonePlayer)
        {
            rutaDB = Application.dataPath + "/Raw/" + DBFileName;
        }
        // Si es android
        else if (Application.platform == RuntimePlatform.Android)
        {
            rutaDB = Application.persistentDataPath + "/" + DBFileName;
            // Comprobar si el archivo se encuantra almacenado en persistant data
            if (!File.Exists(rutaDB))
            {
                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + DBFileName);
                while (!loadDB.isDone)
                { }
                File.WriteAllBytes(rutaDB, loadDB.bytes);
            }
        }
        // ALmaceno el archivo en load db 
        // copio el archivo a persistant data


        strConexion = "URI=file:" + rutaDB;
        dbConnection = new SqliteConnection(strConexion);
        dbConnection.Open();
    }

    void CerrarDB()
    {
        // Cerrar las conexiones
        reader.Close();
        reader = null;
        dbCommand.Dispose();
        dbCommand = null;
        dbConnection.Close();
        dbConnection = null;
    }

    void ComandoSelect(string item, string tabla)
    {
        // Crear la consulta
        dbCommand = dbConnection.CreateCommand();
        string sqlQuery = "SELECT" + item + " FROM " + tabla;
        dbCommand.CommandText = sqlQuery;


        // Leer la base de datos
        reader = dbCommand.ExecuteReader();
        while (reader.Read())
        {
            try
            {

                Debug.Log(reader.GetInt32(0) + " " + reader.GetString(1) + " " + reader.GetInt32(2));
            }
           
            catch (FormatException fe)
            {
                Debug.Log(fe.Message);
                continue;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
                continue;
            }

        }
    }

    void ComandoWHERE(string item, string tabla, string campo, string comparador, string dato)
    {
        // Crear la consulta
        dbCommand = dbConnection.CreateCommand();
        string sqlQuery = "SELECT" + item + " FROM " + tabla + " where " + campo + " " + comparador + " " + dato;
        dbCommand.CommandText = sqlQuery;


        // Leer la base de datos
        reader = dbCommand.ExecuteReader();
        while (reader.Read())
        {
            try
            {

                Debug.Log(reader.GetInt32(0) + " " + reader.GetString(1) + " " + reader.GetInt32(2));
            }

            catch (FormatException fe)
            {
                Debug.Log(fe.Message);
                continue;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
                continue;
            }

        }
    }

    void ComandoWHERE_AND(string item1, string item2, string item3, string item4,
                            string tabla,
                            string campo1, string comparador1, string dato1,
                            string campo2, string comparador2, string dato2)
    {
        // Crear la consulta
        dbCommand = dbConnection.CreateCommand();
        string sqlQuery = "SELECT" + item1 +"," + item2 + "," + item3 + "," + item4 +
                          " FROM " + tabla +
                          " where " + campo1 + " " + comparador1 + " " + "'"+ dato1 + "'" +
                          " and " + campo2 + " " + comparador2 + dato2;
        dbCommand.CommandText = sqlQuery;

        // Leer la base de datos
        reader = dbCommand.ExecuteReader();
        while (reader.Read())
        {
            try
            {

                Debug.Log(reader.GetInt32(0) + " " + reader.GetString(1) + " "+ reader.GetString(2) + reader.GetString(3));
            }

            catch (FormatException fe)
            {
                Debug.Log(fe.Message);
                continue;
            }
            catch (Exception e)
            {
                Debug.Log(e.Message);
                continue;
            }

        }
    }
}
