﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartDB : MonoBehaviour
{
    public GameObject rankingManagerGO;

    void Start()
    {
        rankingManagerGO.GetComponent<RankingManager>().BorrarPuntosExtra();
        rankingManagerGO.GetComponent<RankingManager>().MostrarRanking();
    }

}
