﻿using Mono.Data.Sqlite;
using System.Data;
using System.IO;
using UnityEngine;
using System;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System.Collections;

public class InsertScore : MonoBehaviour
{
    string nombreDB = "RankingDB.db";
    string rutaDB;
    string conexion;
    public Text n;
    public Text s;
    private RankingManager rm;
    public GameObject waitForDBCanvas;
    IDbConnection conexionDB;
    IDbCommand comandosDB;

    private void Start()
    {
        rm = FindObjectOfType<RankingManager>();
    }
    public void AbrirDB()
    {
        if (Application.platform == RuntimePlatform.WindowsEditor || Application.platform == RuntimePlatform.OSXEditor) 
        {
            rutaDB = Application.dataPath + "/StreamingAssets/" + nombreDB;
        }
        else if (Application.platform == RuntimePlatform.Android)
        {
            rutaDB = Application.persistentDataPath + "/" + nombreDB;
            if (!File.Exists(rutaDB))
            {
                WWW loadDB = new WWW("jar:file://" + Application.dataPath + "!/assets/" + nombreDB);
                while (!loadDB.isDone)
                { }
                File.WriteAllBytes(rutaDB, loadDB.bytes);
            }
        }
        // Almaceno el archivo en load db 
        // Copio el archivo a persistant data
        conexion = "URI=file:" + rutaDB;
        conexionDB = new SqliteConnection(conexion);
        conexionDB.Open();
    }
    /*
      el Método Save Score guarda el texto de la puntuación obtenida y el valor del input que escriba el usuario
      y lo inserta en la BBDD
    */
    public void SaveScore() 
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        Debug.Log("Puntuacion: " + n.GetComponent<Text>().text + " s: " + s.GetComponent<Text>().text);
        string sqlQuery = String.Format("insert into Ranking(Name,Score) values(\"{0}\",\"{1}\")", n.GetComponent<Text>().text, s.GetComponent<Text>().text);
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
    }
    IEnumerator WaitForBD() //tiempo para que Triz no sea herida
    {
        yield return new WaitForSeconds(3f);
        SceneManager.LoadScene("MainMenu2");
    }
    public void DeleteScore()
    {
        AbrirDB();
        comandosDB = conexionDB.CreateCommand();
        Debug.Log("Puntuacion: " + n.GetComponent<Text>().text + " s: " + s.GetComponent<Text>().text);
        string sqlQuery = String.Format("delete from Ranking where Name LIKE (\"{0}\")", n.GetComponent<Text>().text);
        comandosDB.CommandText = sqlQuery;
        comandosDB.ExecuteScalar();
        CerrarDB();
        rm.ObtenerRanking();
        waitForDBCanvas.gameObject.SetActive(true); //revisar
        StartCoroutine(WaitForBD());
    }

   
   public void CerrarDB()
    {
        comandosDB.Dispose();
        comandosDB = null;
        conexionDB.Close();
        conexionDB = null;
    }
}
