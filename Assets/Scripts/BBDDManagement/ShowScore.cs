﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ShowScore : MonoBehaviour
{
    private RB Triz;
    public GameObject scoreText;
    private void Start()
    {
        Triz = FindObjectOfType<RB>();
        showPoints();
    }

    public void showPoints()
    {
        scoreText.GetComponent<Text>().text = Triz.gemas.ToString();
        scoreText.SetActive(true);
        Debug.Log("Estoy mostrando los puntos.");
    }
}
