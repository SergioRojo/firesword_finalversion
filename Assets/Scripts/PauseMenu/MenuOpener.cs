﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MenuOpener : MonoBehaviour
{
    public GameObject Menu, SaveMenu;

    public void OpenMenu()
    {
        if (Menu != null) //abre el menú de pausa y se detiene el tiempo
        {
            Menu.SetActive(true);
            SaveMenu.SetActive(false);
            Time.timeScale = 0f;
        }
    }
}
