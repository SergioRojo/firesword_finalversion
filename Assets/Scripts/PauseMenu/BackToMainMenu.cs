﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class BackToMainMenu : MonoBehaviour
{
    public GameObject pauseCanvas;
    public void tiredToPlay()
    {
        SceneManager.LoadScene("MainMenu2");
        //Application.Quit();
        pauseCanvas.SetActive(false);
        Time.timeScale = 1.0f;
    }
}
