﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Continue : MonoBehaviour
{
    public GameObject Menu;

    public void ContinueFireSwording()
    {
        Menu.SetActive(false);
        Time.timeScale = 1f;
    }
}
