﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnableTwoCanvas : MonoBehaviour
{
    public GameObject Canvas;
    public GameObject MenuCanvas;
    public void EnableTwo()
    {
        if (Canvas != null)
        {
            Canvas.SetActive(true);
        }
        if (MenuCanvas != null)
        {
            MenuCanvas.SetActive(true);
        }
    }
}
