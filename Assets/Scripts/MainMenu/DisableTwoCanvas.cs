﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DisableTwoCanvas : MonoBehaviour
{
    public GameObject Canvas;
    public GameObject MenuCanvas;
    public void DisableTwo()
    {
        if (Canvas != null)
        {
            Canvas.SetActive(false);
        }
        if (MenuCanvas != null)
        {
            MenuCanvas.SetActive(false);
        }
    }
}
