﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shop : MonoBehaviour
{
   public GameObject shopPanel;
    public int currentSelectedItem; //registro de cual es el item actual seleccionado
    public int currentItemCost;

    private RB player;
   
   void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
          
             player = other.GetComponent<RB>();
            
            if (player != null)
            {
                //pasamos datos al UIManager
                UIManager.Instance.OpenShop(player.gemas);
            }
            shopPanel.SetActive(true);
           
           // GameObject.FindGameObjectWithTag("GameController").GetComponent<MobileSingleStickControl>().interactable = false;
        }
    }

    private void OnTriggerExit2D(Collider2D other)
    {
        if (other.CompareTag("Player"))
        {
            shopPanel.SetActive(false);
        }
    }

    public void SelectItem(int item)
    {
        Debug.Log("SelectedItem() " + item);

        switch (item)
        {
            case 0:
                UIManager.Instance.UpdateShopSelection(65);
                currentSelectedItem = 0;
                currentItemCost = 150;
                break;
            case 1:
                UIManager.Instance.UpdateShopSelection(-45);
                currentSelectedItem = 1;
                currentItemCost = 110;
                break;
            case 2:
                UIManager.Instance.UpdateShopSelection(-142);
                currentSelectedItem = 2;
                currentItemCost = 200;
                break;
        }
    }

    public void BuyItem()
    {
        if (player.gemas >= currentItemCost)
        {
            if (currentSelectedItem == 2)
            {
                GameManager.Instance.TienesPelo = true;
            }
            player.gemas -= currentItemCost;
            Debug.Log("te has pillao" + currentSelectedItem);
            Debug.Log("te quedan" + player.gemas);
            shopPanel.SetActive(false);
        }
        else
        {
            Debug.Log("necesitas mas money primo");
            shopPanel.SetActive(false);
        }
    }
}
