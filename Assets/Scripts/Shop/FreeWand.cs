﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FreeWand : MonoBehaviour
{
    public float visionRadius;
    protected RB player;
    private List<GameObject> weapons;
    public int activeWeapon;
    public GameObject inventoryPanel;
    public Button inventoryButton;
    public GameObject _fireWandDamage;
    private bool chekeo = false;

    public List<GameObject> GetAllWeapons()
    {
        return weapons;
    }

    private void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
        weapons = new List<GameObject>();
    }
     
    void Update()
    {
        WeaponManager wmanager = FindObjectOfType<WeaponManager>();
        List<GameObject> weapons = wmanager.GetAllWeapons();
        float distance = Vector3.Distance(player.transform.localPosition, transform.localPosition);
        if (distance < visionRadius) //mejor hacerlo con OnTriggerEnter
        {
           if (chekeo == false)
            {
                weapons.Add(_fireWandDamage);
                Button tempB = Instantiate(inventoryButton, inventoryPanel.transform);
                tempB.GetComponent<InventoryButton>().type = InventoryButton.ItemType.WEAPON;
                tempB.GetComponent<InventoryButton>().itemId = 1;
                tempB.onClick.AddListener(() => tempB.GetComponent<InventoryButton>().ActivateButton());
                tempB.image.sprite = _fireWandDamage.GetComponent<SpriteRenderer>().sprite;
                chekeo = true;
                //weapons[1].SetActive(true);
            }

        }
    }

    void OnDrawGizmos()
    {
        Gizmos.color = Color.yellow;
        Gizmos.DrawWireSphere(transform.position, visionRadius);
    }

   /* void OnTriggerEnter2D(Collider2D other)  mejor hacerlo asi
    {
        if (other.CompareTag("Player"))
        {

            player = other.GetComponent<RB>();

            if (player != null)
            {

            }
        }
    }*/
}
