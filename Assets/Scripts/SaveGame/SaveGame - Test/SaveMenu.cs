﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SaveMenu : MonoBehaviour
{
    public GameObject Menu;
    public void OpenMenu()
    {
        if (Menu != null) //abre el menú de guardado y se detiene el tiempo
        {
            Menu.SetActive(true);
            Time.timeScale = 0f;
        }
    }
}
