﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

//Clase que guarda / carga los datos de un componente
public class GestorDeEstado : MonoBehaviour
{
    public static void Guardar(MonoBehaviour componente, int slot = 1)
    {
         
    // así sería sin usar el serializador:
    // Estado estado = (Estado)componente; //se podria tb 'componente as Estado'
    // PlayerPrefs.SetInt("Nivel", estado.Nivel);
    // PlayerPrefs.SetString("slot"+slot, JsonUtility.ToJson(componente));

    /*RB estado = (RB)componente;

   / PlayerPrefs.SetInt("Vidas", estado.Health);
    PlayerPrefs.SetInt("Gemas", estado.gemas);*/
    RB estado = (RB)componente;
        int scene = SceneManager.GetActiveScene().buildIndex;
        int gems = estado.gemas;
        int lifes = estado.Health;
        PlayerPrefs.SetInt("Nivel", scene);
        PlayerPrefs.SetInt("Gemas", gems);
        Debug.Log("Gemas guardadas" + gems);   
        PlayerPrefs.SetInt("Vidas", lifes);
        Debug.Log("Vidas guardadas" + lifes);
    }
    public void crearTriz()
    {
        
    }
        public static void Cargar(MonoBehaviour componente, int slot = 1)
    {
        /* Así sería sin utilizar el serializador Json:
         * Estado estado = (Estado)componente; //se podria tb 'componente as Estado'
         estado.Nivel = PlayerPrefs.GetInt("Nivel");
         estado.Vidas = PlayerPrefs.GetInt("Vidas");*/
        //JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString("slot" + slot), componente);
        //Estado estado = (Estado)componente; //se podria tb 'componente as Estado'

        /*RB estado = (RB)componente;
        estado.Health = PlayerPrefs.GetInt("Vidas");
        estado.gemas = PlayerPrefs.GetInt("Gemas");
        //Triz.gemas = estado.Gemas;
        //Triz.Health = estado.Vidas;*/
        int nivel = PlayerPrefs.GetInt("Nivel");
        RB estado = (RB)componente;
        UIManager canvas  = FindObjectOfType<UIManager>();
        //si escena !3 --> se instancia Triz
        /* if (nivel != 3)
         {
             Instantiate(estado);
         }*/
        SceneManager.LoadScene(PlayerPrefs.GetInt("Nivel"));
        switch (nivel)
        {
            case 0:
                Debug.Log("Has cargado la escena con el nombre MainMenu");
                break;
            case 3:
                Debug.Log("Has cargado el nivel 1");
                estado.nextUuid = "Scene1";
                break;
            case 4:
                Debug.Log("Has cargado el nivel 2");
                estado.nextUuid = "Scene2";
                break;
            case 5:
                Debug.Log("Has cargado el nivel 3");
                estado.nextUuid = "Scene3";
                break;
            default:
                Debug.Log("El gestor de estado no te esta pillando la escena");
                break;
        }
        Debug.Log("Se ha cargado la escena" + PlayerPrefs.GetInt("Nivel"));
        PlayerPrefs.GetInt("Gemas");
        estado.gemas = PlayerPrefs.GetInt("Gemas"); //situar en Triz y en el canvas las gemas y la vida que le corresponden
        canvas.gemCountText.text = estado.gemas.ToString();
        estado.Health = PlayerPrefs.GetInt("Vidas");
        for (int i = 0; i <= 3; i++)
        {

            canvas.healthBars[i].enabled = false;

        }
        for (int i = 0; i < estado.Health; i++)
        {

            canvas.healthBars[i].enabled = true;

        }
        PlayerPrefs.GetInt("Vidas");
        Debug.Log("Las gemas que han cargado son: " + (PlayerPrefs.GetInt("Gemas")));
        Debug.Log("Las vidas que han cargado son: " + (PlayerPrefs.GetInt("Vidas"))); 
    }
}
