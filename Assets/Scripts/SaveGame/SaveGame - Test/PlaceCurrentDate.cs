﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class PlaceCurrentDate : MonoBehaviour
{
    public Text textScene,textTime;

    public void timeIsNow()
    {
        string time = System.DateTime.UtcNow.ToLocalTime().ToString("yyyy/MM/dd HH:mm:ss");
        int scene = SceneManager.GetActiveScene().buildIndex;
        string placeName;

        switch (scene)
        {
            case 3:
                placeName = "Gruta Antigua -Entrada";
                break;
            case 4:
                placeName = "Gruta Antigua -Ruinas arcaicas";
                break;
            case 5:
                placeName = "Gruta Antigua       -" +
                    "Túneles Cavernarios";
                break;
            default:
                placeName = "Última partida guardada";
                break;
        }
        textTime.text = time;
        textScene.text = placeName;
        ;
    }
}
