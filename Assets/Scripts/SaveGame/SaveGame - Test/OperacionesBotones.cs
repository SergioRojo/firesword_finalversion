﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperacionesBotones : MonoBehaviour
{
    //public Estado estado;
    public RB estado; //??? es necesario para que funcione aunque ahora mismo no sepa muy bien por qué
    public GameObject canvas;

    public void Guardar(int slot)
    {
        GestorDeEstado.Guardar(estado, slot);
        Debug.Log("Se ha GUARDADO en el slot " + slot);
    }
    
   public void Cargar(int slot)
    {
        canvas.SetActive(true);
        estado.GetComponentInChildren<SpriteRenderer>().enabled = true;
        GestorDeEstado.Cargar(estado, slot);
        Debug.Log("Se ha CARGADO en el slot " + slot);
        
        //quizá estado.nivel --> Scene.LoadScene(estado.nivel)?
    }
}
