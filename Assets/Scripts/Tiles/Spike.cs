﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Spike : MonoBehaviour
{
    private RB player;
    public bool _canDamage = true;

    void Start()
    {
        player = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
    }
    public void OnTriggerEnter2D(Collider2D other)
    {
        IDamageable hit = other.GetComponent<IDamageable>();

        if (hit != null)
        {
            if (_canDamage == true)
            {
                hit.Damage();
                hit.Damage();
                _canDamage = false;
                StartCoroutine(ResetDamage());
               //StartCoroutine(player.Knockback(0.02f, 350, player.transform.position));
            }
        }
    }

    public virtual IEnumerator ResetDamage()
    {
        yield return new WaitForSeconds(0.3f); //tiempo de espera para que los enemigos puedan recibir daño!!!

        _canDamage = true;
    }
}
