﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
public class MainMenu : MonoBehaviour
{
    private RB Triz;
    public Vector3 posInicial = new Vector3(-554, 10, 0);
    private UIManager canvas;
    //public RB trizSprite;

    void Start()
    {
        Triz = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
        canvas = FindObjectOfType<UIManager>();
    }
    public void StartButton()
    {
        Triz.nextUuid = "Scene1";
        SceneManager.LoadScene("1.1");
        canvas.gameObject.SetActive(true);
        if (Triz == null)
        {
            Debug.Log("No hay Triz");
        }else
        {
            Debug.Log("Hay Triz");
            Triz.transform.position = posInicial;
            Triz.GetComponentInChildren<SpriteRenderer>().enabled = true;
        }
    }

    public void QuitButton()
    {
        Application.Quit();
    }
}
