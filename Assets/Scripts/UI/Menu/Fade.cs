﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade : MonoBehaviour
{
    public Image fade;
    void Start()
    {
        fade.CrossFadeAlpha(0, 2, true);
    }

 
}
