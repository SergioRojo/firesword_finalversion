﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class UIManager : MonoBehaviour //gestión de la UI de la tienda y la mayoría de elementos UI del juego
{
    private static UIManager _instance;
    public static bool healthcr;
    public Image trizImage;  //cambiar la cara de Triz cuando recibe daño
    public GameObject trizImageHit;
    public static UIManager Instance
    {
        get
        {
            if (_instance == null)
            {
                Debug.LogError("UI MANAGER IS NULL");
            }
            return _instance;
        }
    }

    public Text playerGemCountText; //muestra las gemas de player
    public Image selectionImg;
    public Text gemCountText; //para que se vea el contador de gemas en la pantalla
    public Image[] healthBars;
    public Image weaponButton;
    private WeaponManager weaponmanag;


    private void Start()
    {
        weaponmanag = FindObjectOfType<WeaponManager>();
        StartCoroutine(DisableCanvas());
    }

    IEnumerator DisableCanvas()//Is necessary that the canvas was actived at start in order to set his DontDestroyOnLoad active    
    {
        yield return new WaitForSeconds(0.002f);
        this.gameObject.SetActive(false);
    }

    private void Awake()
    {
        _instance = this; //para que no sea null
    }

    public void OpenShop(int gemCount) //al abrir la tienda las gemas que tendrá el player se pasan por parámetro
    {
        playerGemCountText.text = "" + gemCount + " Paveles";
    }
    
    public void UpdateShopSelection(int yPos) //le pasamos la posición en la cual se encuentra el selection (por variar, lo lógico quizá hubiera sido pasarle el int item)
    {
        selectionImg.rectTransform.anchoredPosition = new Vector2(selectionImg.rectTransform.anchoredPosition.x, yPos);
    }

    public void UpdateGemCount(int count)
    {
        gemCountText.text = "" + count;
    }

    public void UpdateLives(int livesRemaining)
    {
        for (int i = 0; i <= livesRemaining; i++)
        {
            if (i == livesRemaining) //está en la última barra, así que la ocultará
            {
                healthBars[i].enabled = false;
            }
        }
    }

    public GameObject inventoryPanel, menuPanel;
    public Button inventoryButton;
    public GameObject HUD;
    public void ToggleInventory()
    {
        inventoryPanel.SetActive(!inventoryPanel.activeInHierarchy); //active in Hierarchy devuelve true si está activo y false si desactivado, entonces devolvemos lo contrario a lo que esté
        menuPanel.SetActive(!menuPanel.activeInHierarchy);
        if (inventoryPanel.activeInHierarchy)
        {
            foreach (Transform t in inventoryPanel.transform)
            {
                Destroy(t.gameObject);
            }
            FillInventory();         
        }
        if (HUD.activeInHierarchy)
        {
            HUD.SetActive(false);
        }
        else
        {
            HUD.SetActive(true);
        }
    }

    public void FillInventory() //así se recuperan todas las armas
    {
        ItemManager imanager = FindObjectOfType<ItemManager>();

        List<GameObject> weapons = weaponmanag.GetAllWeapons();
        List<GameObject> items = imanager.GetAllItem();
        int j = 0;
        int i = 0;
       //weapons
        foreach (GameObject w in weapons)
        {
            Button tempB = Instantiate(inventoryButton, inventoryPanel.transform);
            tempB.GetComponent<InventoryButton>().type = InventoryButton.ItemType.WEAPON;
            tempB.GetComponent<InventoryButton>().itemId = i;
            tempB.onClick.AddListener(() => tempB.GetComponent<InventoryButton>().ActivateButton());
            tempB.image.sprite = w.GetComponent<SpriteRenderer>().sprite;
            i++;
        }
        //items
        foreach (GameObject x in items)
        {
            Button tempB = Instantiate(inventoryButton, inventoryPanel.transform);
            tempB.GetComponent<InventoryButton>().type = InventoryButton.ItemType.ITEM;
            tempB.GetComponent<InventoryButton>().itemId = j;
            tempB.onClick.AddListener(() => tempB.GetComponent<InventoryButton>().ActivateButton());
            tempB.image.sprite = x.GetComponent<SpriteRenderer>().sprite;
            j++;
        }
    }

    public void ShowOnly(int type)
    {
        foreach(Transform t in inventoryPanel.transform)
        {
            t.gameObject.SetActive((int)t.GetComponent<InventoryButton>().type == type);
        }
    }

    public void ShowAll()
    {
        foreach(Transform t in inventoryPanel.transform)
        {
            t.gameObject.SetActive(true);
        }
    }

    public void ChangeWeaponImage(Sprite sprite)
    {
        weaponButton.sprite = sprite; 
    }

    public void ChangeTrizImage()
    {
        trizImage.gameObject.SetActive(false) ;
        trizImageHit.gameObject.SetActive(true);
    }

    public void CambiarCara()
    {
        trizImage.gameObject.SetActive(true);
        trizImageHit.gameObject.SetActive(false);
    }

    public void Jetazo()
    {
        Invoke("CambiarCara", .5f);
    }
}
