﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InventoryButton : MonoBehaviour //rste script se encarga de diferenciar los botones del inventario, famili a la que pertenecen...
{
    public enum ItemType { WEAPON = 0, ITEM = 1, SHIELD = 2, ARMOR = 3, BOOTS = 4}; //si quisieramos ARMOR, RING.. PUES METER
    public int itemId;
    public ItemType type;
    
    public void ActivateButton()
    {
        switch (type)
        {
            case ItemType.WEAPON: //en caso de que tenga un botón de tipo arma, llamamos al changeweapon
                FindObjectOfType<WeaponManager>().ChangeWeapon(itemId);
                break;
            case ItemType.ITEM: //en caso de que tenga un botón de tipo item, llamamos al changeweapon
                
                FindObjectOfType<ItemManager>().ChangeItem(itemId);
                Debug.Log("en futuros DLC");
                break;

        }
    }
}
