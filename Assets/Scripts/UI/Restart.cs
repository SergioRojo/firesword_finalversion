﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


    public class Restart : MonoBehaviour
    {
        private RB triz;
        private UIManager canvas;
    public GameObject pauseCanvas; 

    private void Start()
    {
        triz = FindObjectOfType<RB>();
        canvas = FindObjectOfType<UIManager>();
    }
    public void RestartGame()
    {
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
        Time.timeScale = 1f;
        triz.gemas = 0;
        triz.Health = 4;
        for (int i = 0; i <= 3; i++)
        {

            canvas.healthBars[i].enabled = true;

        }
        canvas.gemCountText.text = "0";
        pauseCanvas.SetActive(false);
    }

}

