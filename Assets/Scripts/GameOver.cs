﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class GameOver : MonoBehaviour
{
    public GameObject gameOver, restartButton, infotext;
    private RB triz;
    private Camera cam;
    private UIManager canvas;
    private RBAnimation _playerAnim;
    public static GameOver instance;

    private void Start()
    {
        triz = FindObjectOfType<RB>();
        _playerAnim = FindObjectOfType<RBAnimation>();
        canvas = FindObjectOfType<UIManager>();
        //I think this both methods are useless right now
            //Level1();
           // OtherLevels();
    }

    public void Level1()
    {
        restartButton.SetActive(false);
        infotext.SetActive(true);
    }
    public void OtherLevels()
    {
        restartButton.SetActive(true);
        infotext.SetActive(false);
    }
    public void ReStartButton()
    {
            triz.isDead = false;
            triz.speed = 4.0f; //debería ser 5? 
            triz.gemas = 0;
            triz.Health.Equals(4);
            //triz.nextUuid
            _playerAnim.OneStep(true);
            for (int i = 0; i <= 3; i++)
            {

                canvas.healthBars[i].enabled = true;

            }
            for (int i = 0; i < 4; i++)
            {
                triz.Health++;
            }
            canvas.gemCountText.text = "0";
            SceneManager.LoadScene(PlayerPrefs.GetInt("Nivel"));
    }

    public void QuitButton()
    {
        SceneManager.LoadScene("MainMenu2");
    }
}
