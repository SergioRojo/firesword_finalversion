﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class ChangeScene : MonoBehaviour
{
    public RB triz;
    public string Scene;
    public string uuid;

    void OnTriggerEnter2D(Collider2D other) //cuando esté dentro de un collider empiece la nueva escena
    {
        if(other.CompareTag("Player")) //El Tag del objeto Portal, en la jerarquía, tiene que estar marcado "player"
        {
            Teleport();
        }
    }

    private void Teleport()
    {

        FindObjectOfType<RB>().nextUuid = uuid;
        SceneManager.LoadScene(Scene); //Se añaden las escenas en Build Settings
        //PlayerPrefs.SetInt("Vidas", triz.Health);
        //PlayerPrefs.SetInt("Gemas", triz.gemas);
    }
}
