﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu2 : MonoBehaviour
{
    private RB Triz;
    private UIManager canvas;
    private RBAnimation _playerAnim;
    public Vector3 posInicial = new Vector3(-554, 10, 0);

    void Start()
    {
        Triz = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
        _playerAnim = FindObjectOfType<RBAnimation>();
        canvas = FindObjectOfType<UIManager>();
    }
    public void StartButton()
    {
        Triz.nextUuid = "Scene1";
        Triz.isDead = false;
        Triz.speed = 4.0f; //debería ser 5? 
        Triz.gemas = 0;
        Triz.Health.Equals(4);
        _playerAnim.OneStep(true);
        for (int i = 0; i <= 3; i++)
        {

            canvas.healthBars[i].enabled = true;

        }
        for (int i = 0; i < 4; i++)
        {
            Triz.Health++;
        }
        canvas.gemCountText.text = "0";
        SceneManager.LoadScene("1.1");
        if (Triz == null)
        {
            Debug.Log("No hay Triz");
        }
        else
        {
            Debug.Log("Hay Triz");
            Triz.transform.position = posInicial;
        }
    }
    public void QuitButton()
    {
        Application.Quit();
    }
}
