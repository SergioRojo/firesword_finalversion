﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OperacionesBotones2 : MonoBehaviour
{
    private RB estado;
    private UIManager canvas;
    void Start()
    {
        estado = FindObjectOfType<RB>();
        canvas = FindObjectOfType<UIManager>();
        if (estado == null)
        {
            Debug.Log("No hay estado");
        }
        else
        {
            Debug.Log(" hay estado");
        }
        if (canvas == null)
        {
            Debug.Log("No hay canvas");
        }
        else
        {
            Debug.Log("hay canvas");
        }
    }
    public void Guardar(int slot)
    {
        GestorDeEstado.Guardar(estado, slot);
        Debug.Log("Se ha GUARDADO en el slot " + slot);
    }

    public void Cargar(int slot)
    {
        canvas.gameObject.SetActive(true);
        estado.GetComponentInChildren<SpriteRenderer>().enabled = true;
        GestorDeEstado.Cargar(estado, slot);
        Debug.Log("Se ha CARGADO en el slot " + slot);

        //quizá estado.nivel --> Scene.LoadScene(estado.nivel)?
    }
}
