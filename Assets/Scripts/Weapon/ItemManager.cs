﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ItemManager : MonoBehaviour
{
    private List<GameObject> items;
    public int activeItem;

    public List<GameObject> GetAllItem()
    {
        return items;
    }
    void Start()
    {
        items = new List<GameObject>();
        foreach (Transform item in transform)
        {
          // items.Add(item.gameObject);
        }
        for (int i = 0; i < items.Count; i++)
        {
            items[i].SetActive(i == activeItem); //le pasas el resultado del booleano, si la i = al arma activa da true, sino false
        }
    }

    public void ChangeItem(int newItem)
    {
        items[activeItem].SetActive(false);
        items[newItem].SetActive(true);
        activeItem = newItem;
    }
}
