﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

public class TrueFire : MonoBehaviour  //script bola de fuego    SPRITES BOLAS!!!: ESPECIAL free_effects -> Fireball    normal: gameFXexport fireball64x64
{
    protected Enemy enemy;
    Vector3 direction;
    protected SpriteRenderer sprite;
    protected RB Triz; //ponemos aqui RB porque tienes que sacar el hecho de que esté mirando a izquierda o derecha
    private int speed;
    private Vector3 derecha = Vector3.right;
    private Vector3 izquierda = Vector3.left;


    void Start()
    {
        Destroy(this.gameObject, 0.85f);
        enemy = GameObject.FindGameObjectWithTag("Enemy").GetComponent<Enemy>();
        direction = enemy.transform.localPosition - transform.localPosition;
        sprite = GetComponentInChildren<SpriteRenderer>();
    }

    void Update()
    {
        Debug.Log(Time.deltaTime);
        float move = CrossPlatformInputManager.GetAxis("Horizontal");
        //en lugar de right, "direccion" -->
        //if (move > 0)
       // { 
            transform.Translate(Vector3.right * 9 * Time.deltaTime); //si en lugar de Time.deltaTime pones 1f va rapidisimo 
       // }


       /* else if (move < 0)
        {
                transform.Translate(Vector3.left * 9 * Time.deltaTime);
                sprite.flipX = true;
                Debug.Log("Bolazo hacia la izquierda!!!!!");
        }
        else
        {
            transform.Translate(Vector3.right * 9 * Time.deltaTime);
            Debug.Log("Bolazo neutro");
        }*/
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Enemy"))
        {
            IDamageable hit = other.GetComponent<IDamageable>();


            if (hit != null)
            {
                hit.Damage();
                Destroy(this.gameObject);
            }
        }
    }

    /*private float increaseSpeed()
    {

        /* float value = 1;
         for (int x = 1; x < 5; x++)
         {
             value = 1 * x;

         }

         return value;
        float value = 1;
        float currentTime = 0;
        float maxTime = 1;
        currentTime = Time.deltaTime;
        if (currentTime >= maxTime)
        {
            currentTime = 0;
            value++;

           
        }
        return value;

    }*/

  
}
