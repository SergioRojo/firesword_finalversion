﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireBall : MonoBehaviour
{
    private FireWandDamage _fireWandDamage;

    void Start()
    {
        _fireWandDamage = transform.parent.GetComponent<FireWandDamage>();
    }

    public void openFire()
    {
        _fireWandDamage.Fire();
    }
}
