﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FireWandDamage : MonoBehaviour
{
    //SCRIPT BASTON DE FUEGO
    public GameObject FireBall;
    protected RB Triz;
 

    public void Fire()
    {     
            Triz = GameObject.FindGameObjectWithTag("Player").GetComponent<RB>();
            Triz.FireWandAttack();
            Instantiate(FireBall, Triz.transform.position, Quaternion.identity);
    }

}
