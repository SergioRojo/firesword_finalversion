﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WeaponManager : MonoBehaviour
{
    public List<GameObject> weapons;
    public Button basicButton, noWButton, fireWandButton, wndButton;
    public int activeWeapon;
    public GameObject NoneWeapon;

    public List<GameObject> GetAllWeapons()
    {
        return weapons;
    }
    void Start()
    {
        weapons = new List<GameObject>();
        weapons.Add(NoneWeapon.gameObject);
        /*foreach(Transform weapon in transform) //así se muestran todos los objetos existentes
         {
           weapons.Add(weapon.gameObject);
         }*/
        for (int i = 0; i < weapons.Count; i++)
        {
            weapons[i].SetActive(i == activeWeapon); //le pasas el resultado del booleano, si la i = al arma activa da true, sino false
        }
    }

    public void ChangeWeapon(int newWeapon)
    {
        weapons[activeWeapon].SetActive(false);
        weapons[newWeapon].SetActive(true);
        activeWeapon = newWeapon;
        FindObjectOfType<UIManager>().ChangeWeaponImage(weapons[activeWeapon].GetComponent<SpriteRenderer>().sprite);
        switch (activeWeapon)
        {
            case 0:
                basicButton.gameObject.SetActive(false); //quizá con interactable
                noWButton.gameObject.SetActive(true);
                basicButton = noWButton;
            break;
            case 1:
                basicButton.gameObject.SetActive(false);
                fireWandButton.gameObject.SetActive(true);
                basicButton = fireWandButton;
                break;
            case 2:
                basicButton.gameObject.SetActive(false);
                wndButton.gameObject.SetActive(true);
                basicButton = wndButton;
                break;
        }
    }

}
