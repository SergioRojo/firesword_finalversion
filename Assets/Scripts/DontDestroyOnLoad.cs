﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DontDestroyOnLoad : MonoBehaviour
{
    void Start()
    {
        if (!RB.playerCreated)
        {
            DontDestroyOnLoad(this.transform.gameObject);
        }else
        {
            Destroy(gameObject);
        }
       
    }
}
