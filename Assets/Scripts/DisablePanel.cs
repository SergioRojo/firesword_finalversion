﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisablePanel : MonoBehaviour
{
    public GameObject LoadMenu;
    public Image fade;
    public void DisableMenu()
    {
        if (LoadMenu != null) 
        {
            fade.CrossFadeAlpha(1, 1, true);
            LoadMenu.SetActive(false);
        }
    }
}
