﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public  class Attack : MonoBehaviour
{
    public bool _canDamage = true;
    public void OnTriggerEnter2D(Collider2D other)
    {
        IDamageable hit = other.GetComponent<IDamageable>();

        if (hit != null)
        {
            if (_canDamage == true)
            {
                hit.Damage();
                _canDamage = false;
                StartCoroutine(ResetDamage());
            }
        }
    }

    public virtual IEnumerator ResetDamage()
    {
        yield return new WaitForSeconds(0.3f); //tiempo de espera para que los enemigos puedan recibir daño!!!

        _canDamage = true;
    }
}
