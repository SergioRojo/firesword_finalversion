﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InstructionCanvasManager : MonoBehaviour
{
    public GameObject panel1, panel2, panel3;
    public GameObject canvas;
    //Touch touch = Input.GetTouch(0);
    Boolean vd = true;
    private void Start()
    {
        Time.timeScale = 0f;
    }
    void Update()
    {
        continueInstructions();
    }

   public void continueInstructions()
    {
       if ((Input.touchCount == 1) && (vd == true))
        {
            Debug.Log("Hay un toque");
            StartCoroutine(NewPanel());
        }
        else if ((Input.touchCount == 1) && (vd == false))
        {
            panel3.SetActive(false);
            canvas.SetActive(false);
            panel1.SetActive(false);
            panel2.SetActive(false);
            Debug.Log("Hay dos toques");
            
        }
    }

    IEnumerator NewPanel()
    {
        Time.timeScale = 1f;
        yield return new WaitForSeconds(1.5f);
        vd = false;
        panel1.SetActive(false);
        panel2.SetActive(false);
        panel3.SetActive(true);

    }
    
}
